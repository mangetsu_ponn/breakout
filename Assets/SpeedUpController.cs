﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedUpController : MonoBehaviour
{
    GameObject scoreDirector;
    GameObject ball;
    ScoreDirector scoreDirectorScript;
    BallController ballController;

    // Start is called before the first frame update
    void Start()
    {
        scoreDirector = GameObject.Find("ScoreDirector");
        scoreDirectorScript = scoreDirector.GetComponent<ScoreDirector>();
        ball = GameObject.Find("Ball");
        ballController = ball.GetComponent<BallController>();
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Ball")
        {
            collision.gameObject.GetComponent<Rigidbody>().AddForce(
            (transform.forward + transform.right) * 18.0f, ForceMode.VelocityChange);
            scoreDirectorScript.score += 1000;
            Destroy(this.gameObject);
        }
    }
}
