﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDirector : MonoBehaviour
{
    public GameObject BlockPrefab;

    // Start is called before the first frame update
    void Start()
    {
        for(int j = 0; j < 7; j++)
        {
            for(int i = 0; i < 4; i++)
            {
                if(i != 1 && i != 2)
                {
                    Instantiate(BlockPrefab,
                        new Vector3(-8.1f + j * 2.7f, 0, 24.0f + i * (-1.5f)), Quaternion.identity);
                }

            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
