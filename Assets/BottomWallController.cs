﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BottomWallController : MonoBehaviour
{
    GameObject camera;
    AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(collision.gameObject);

        camera = GameObject.Find("Main Camera");
        audioSource = camera.GetComponent<AudioSource>();
        audioSource.Stop();

        SceneManager.LoadScene("GameOverScene");

    }

}
