﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow) && this.transform.position.x <= 6.7f)
        {
            transform.position += new Vector3(0.55f, 0, 0);
        }

        if (Input.GetKey(KeyCode.LeftArrow) && this.transform.position.x >= -6.7f)
        {
            transform.position += new Vector3(-0.55f, 0, 0);
        }
    }
}
