﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallCreateController : MonoBehaviour
{
    GameObject scoreDirector;
    GameObject ball;
    ScoreDirector scoreDirectorScript;

    // Start is called before the first frame update
    void Start()
    {
        scoreDirector = GameObject.Find("ScoreDirector");
        scoreDirectorScript = scoreDirector.GetComponent<ScoreDirector>();
        ball = GameObject.Find("Ball");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Ball")
        {
            Instantiate(ball, new Vector3(8.2f, 0, 15.0f), Quaternion.identity);
            scoreDirectorScript.score += 1000;
            Destroy(this.gameObject);
        }
    }
}
