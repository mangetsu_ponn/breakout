﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BallController : MonoBehaviour
{
    public float speed = 13.0f;
    int blockCount = 16;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody>().AddForce(
            (transform.right - transform.forward) * speed, ForceMode.VelocityChange);
    }

    // Update is called once per frame
    void Update()
    {
       if(blockCount == 0)
        {
            SceneManager.LoadScene("ClearScene");
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Block")
        {
            blockCount -= 1;
        }
    }
}
