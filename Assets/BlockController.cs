﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockController : MonoBehaviour
{
    GameObject scoreDirector;
    ScoreDirector scoreDirectorScript;

    // Start is called before the first frame update
    void Start()
    {
        scoreDirector = GameObject.Find("ScoreDirector");
        scoreDirectorScript = scoreDirector.GetComponent<ScoreDirector>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Ball")
        {
            scoreDirectorScript.score += 100;
            Destroy(this.gameObject);
        }
    }
}
